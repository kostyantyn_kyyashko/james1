<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
$memcached = new Memcached();
$memcached->addServer('localhost', 11211);

$memcached->set('key', 'value');

echo $memcached->get('key');
