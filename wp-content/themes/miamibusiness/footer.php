	<footer>

		<div class="footer">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-3">

						<p>Miami Business Magazine - 2018</p>
						
					</div>

					<div class="col-xs-12 col-sm-4 pull-right">						

						<nav>
							<ul>							
								<li><a href="<?= site_url() ?>/contact"> Contact</a></li>
								<li class="separator"></li>
								<li><a href="<?= site_url() ?>/advertising"> Advertising</a></li>
							</ul>
						</nav>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</footer>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121555628-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121555628-1');
</script>


	<script src="<?= get_template_directory_uri() ?>/js/jquery.js"></script>
	<script src="<?= get_template_directory_uri() ?>/js/reveal.js"></script>

	</body>

</html>