<?

get_header();

?>

	<section>

		<div class="contact-page-banner">

			
		</div>
		
	</section>

	<section>

		<div class="contact-page">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-6">

						<h2>Advertising rates for Bellevue Chronicle</h2>

						<h3>Contact Daniel Brown atpress@bellevuechronicle.com for more advertising information.</h3>

						<p>Thank you for your interest in partnering with us. Bellevue Chronicle offers advertisers local knowledge and regional character dovetailed with a local and targeted city reach.</p>

						<p>Our unrivaled local intelligence, interactive digital brand extensions and high touch events make us the most efficient way for brands to reach America’s most livable city.</p>					

						<p>Since 2006, The Bellevue Chronicle has been a trusted resource of information for consumers and residents of Bellevue, Washington. Our writers focus on content that speaks to the concerns of the issues of Bellevue, and some of Seattle!</p>

						<p>Our writers are UW students, BCC students, and several residents of Bellevue and Seattle, Washington that want to write real stories that are engaging and entertaining.</p>
						
					</div>

					<div class="col-xs-12 col-sm-6">

						<p>The Bellevue Chronicle offers monthly editorials that feature our comprehensive calendar of events, inspiring real life stories of local residents, expert opinions, categorized resources of information and things to do and we cover a little bit of everything.</p>

						<p>This is why our readership has come to count on Bellevue Chronicle for the information that they need to know.</p>

						<p>We are ever developing our interactive website to make it more user-friendly and informative for our viewers and we are bringing in new writers constantly.</p>

						<p>At Bellevue Chronicle, we think of all our advertisers as part of our extended family. For this reason, we have developed a number of value added opportunities for our advertisers that include Full color ads, Ad design, and digital ads for homepage and secondary pages.</p>

						<p>For more information or to request advertising rates, please shoot us an email to the above email and we will send you a confirmation email our ad rates for the media the media prices that you have requested.</p>

						<p>Someone will be in touch with you soon!</p>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</section>

	<script src="<?= get_template_directory_uri() ?>/js/home.js"></script>

<?

get_footer();