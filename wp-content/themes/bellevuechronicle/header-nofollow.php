<?

global $category;

$categories = get_categories(array('hide_empty' => 0, 'orderby' => 'ID'));

?>

<!DOCTYPE html>
<html lang="en" class="no-skrollr">

<head>		
	<meta charset="UTF-8"/>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<title><?= $post -> post_title . ' - ' ?><? bloginfo('name') ?></title>

	<meta name="description" content="For more than 23 years, Bellevue Chronicle has captured our city with award-winning writing and groundbreaking reporting">

	<meta name="robots" content="noindex,nofollow">

	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/bootstrap.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/global.css?v=<?= uniqid() ?>">

	<link rel="shortcut icon" type="image/png" href="<?= IMG ?>favicon.ico"/>

	<meta property="og:url"                content="" />
	<meta property="og:type"               content="website" />
	<meta property="og:title"              content="" />
	<meta property="og:description"        content="" />
	<meta property="og:image"              content="" />

	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600" rel="stylesheet">

	<script src="<?= get_template_directory_uri() ?>/js/class.js"></script>

</head>

	<body>

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=193366454838334&autoLogAppEvents=1';
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<section>

			<div class="display-bar">

				<div class="container">

					<div class="row">

						<div class="col-xs-12 col-sm-8">

							<p>Advertise with us <a href="<?= site_url() ?>/advertising">Learn more <i class="fa fa-chevron-right"></i></a></p>
							
						</div>

						<div class="col-xs-12 col-sm-4">

							<ul>
								<li><a href="https://www.facebook.com/BellevueChronicle/"><i class="fab fa-facebook"></i></a></li>								
								<li><a href="mailto:contact@bellevuechronicle.com"><i class="fas fa-envelope"></i></a></li>
							</ul>
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			
		</section>

		<header>

			<div class="header">

				<div class="container">

					<div class="row">

						<div class="col-xs-12 col-sm-3">

							<a href="<?= home_url(); ?>" class="logo"><img src="<?= get_template_directory_uri() ?>/img/logo.gif" alt=""></a>
							
						</div>

						<div class="col-xs-12 col-sm-9">

							<nav>
								<ul class="nav">
									<li><a href="<?= site_url() ?>/seattle">Seattle</a></li>
									<li><a href="<?= site_url() ?>/eastside">Eastside</a></li>
									<li><a href="<?= site_url() ?>/entertainment">Entertainment</a></li>
								</ul>
							</nav>
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			
		</header>

