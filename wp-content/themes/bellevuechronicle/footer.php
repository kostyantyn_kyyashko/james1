	<footer>

		<div class="footer">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-3">

						<p>Bellevue Chronicle<br>All rights reserved<br>2018</p>

						<nav>
							<ul class="social">
								<li><a href="https://www.facebook.com/BellevueChronicle/" target="_blank"><i class="fab fa-facebook"></i></a></li>								
								<li><a href="mailto:press@bellevuechronicle.com"><i class="fas fa-envelope"></i></a></li>
							</ul>
						</nav>
						
					</div>

					<div class="col-xs-12 col-sm-3">						

						<nav>
							<ul class="nav">
								<li><a href="<?= site_url() ?>/seattle">Seattle</a></li>
								<li><a href="<?= site_url() ?>/eastside">Eastside</a></li>
								<li><a href="<?= site_url() ?>/entertainment">Entertainment</a></li>
							</ul>
						</nav>
						
					</div>

					<div class="col-xs-12 col-sm-2 pull-right">						

						<nav>
							<ul>
								<li><a href="<?= site_url() ?>/terms-of-service"><i class="fa fa-angle-double-right"></i> Terms of Service</a></li>
								<li><a href="<?= site_url() ?>/contact"><i class="fa fa-angle-double-right"></i> Contact</a></li>
								<li><a href="<?= site_url() ?>/advertising"><i class="fa fa-angle-double-right"></i> Advertising</a></li>
							</ul>
						</nav>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</footer>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121555628-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121555628-1');
</script>


	<script src="<?= get_template_directory_uri() ?>/js/jquery.js"></script>
	<script src="<?= get_template_directory_uri() ?>/js/reveal.js"></script>

	</body>

</html>