<?

bellevue_setup();

function bellevue_setup() {

	add_theme_support( 'title-tag' );

	// Add theme support for Featured Images
	add_theme_support('post-thumbnails', array(
	'post',
	'page',
	'custom-post-type-name',
	));

	require_once('ScraperLib.php');

}