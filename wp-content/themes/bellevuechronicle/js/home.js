var App = Class.extend({

	sk: null,

	init: function() {

		var self = this;

		self.bindAnimations();

		self.bindActions();

	},

	bindAnimations: function() {

		var self = this;

		$('.menu-bt').on('click', function() {

			$('.nav').toggle();

		})

		$('.banner .main-article a').hover(function() {

			$(this).find('img').animate({
				opacity: 1
			}, 200);

		}, function() {

			$(this).find('img').animate({
				opacity: .8
			}, 200);

		});

		$('.banner .side-article a').hover(function() {

			$(this).find('img').animate({
				opacity: 1
			}, 200);

		}, function() {

			$(this).find('img').animate({
				opacity: .5
			}, 200);

		});

		$(window).on('scroll', function() {

			// if(window.scrollY > 48) {

			// 	$('.header').addClass('compact-header');

			// }else{

			// 	$('.header').removeClass('compact-header');

			// }

		})
		
	},

	bindActions: function() {

		var self = this;

	}

})

window.onload = function() {

	var app = new App();

}