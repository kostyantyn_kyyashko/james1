<?

$post = get_post('home');

get_header();

function get_image_src($img)
{
    preg_match_all('/(src)=("[^"]*")/i', $img, $y);
    return str_replace('src="', '', $y[0][0]);
}

?>
	<?

		$featuredArticles = get_posts(array(
			'posts_per_page'	=> -1,
			'post_type'			=> 'post',
			'meta_key'			=> 'featured',
			'meta_value'		=> true
		));

		$articles = new WP_Query('post_type=post&posts_per_page=32');
		$articles = (array) $articles -> posts;

		for ($j=0; $j < count($articles); $j++) { 

			$html = html_entity_decode($articles[$j] -> post_content);

			preg_match_all('/<img[^>]+>/i',$html, $result);
            $articles[$j] -> post_cover = [];
			if (is_array($result[0])) {
			    foreach ($result[0] as $img) {
			        $src = get_image_src($img);
                    $articles[$j] -> post_cover[] = str_replace(' ', '_', $src);
                }
            }
/*			print_r($out);
            die();*/
/*			$doc = new DOMDocument();
			$doc -> loadHTML($html);

			$xpath = new DOMXPath($doc);*/
/*            $articles[$j] -> post_cover = [];
            $img = $main_array[$j];
            if (is_array($img)) {
                foreach ($img as $i) {

                    $articles[$j] -> post_cover = $y[0][0];
                }
            }*/
			/*$images = [];



			foreach ($images as $img) {

				$articles[$j] -> post_cover[] = $img -> getAttribute('src');
				
			}

			unset($images);
			*/
		}

	?>

	<section>

		<div class="banner">

			<div class="container-fluid">

				<div class="row">

					<div class="col-xs-12 col-sm-9 clear-pad-both">

						<div class="main-article">
							<?php if ($featuredArticles[1]): ?>
							<a href="<?= site_url() ?>/seattle/<?= $featuredArticles[1] -> post_name ?>">
								<div class="details">
									<h3><?= $featuredArticles[1] -> post_title ?></h3>
									<h5 class="seattle"><i class="fas fa-map-marker-alt"></i> Seattle</h5>

									<h6>Written by <?= date('d, M', strtotime($featuredArticles[1] -> post_date)) ?></h6>
								</div>
								<?= get_the_post_thumbnail( $featuredArticles[1] -> ID, 'full' ) ?>	
							</a>
							<?php endif ?>
						</div>
						
					</div>

					<div class="col-xs-12 col-sm-3 clear-pad-both">

						<div class="row">

							<div class="col-xs-12 clear-pad-both">

								<div class="side-article">
									<a href="<?= site_url() ?>/eastside/<?= $featuredArticles[0] -> post_name ?>">
										<?php if ($featuredArticles[0]): ?>
										<div class="details">
											<h3><?= $featuredArticles[0] -> post_title ?></h3>
											<h5 class="east-side"><i class="fas fa-angle-double-right"></i> East Side</h5>
										</div>
										<img src="<?= get_the_post_thumbnail_url( $featuredArticles[0] -> ID, 'full' ) ?>	" alt="">
										<?php endif ?>	
									</a>
								</div>
								
							</div>
							
						</div>

						<div class="row">

							<div class="col-xs-12 clear-pad-both">

								<div class="side-article">
									<a href="<?= site_url() ?>/eastside/<?= $articles[2] -> post_name ?>">
										<?php if ($articles[2]): ?>
										<div class="details">
											<h3><?= $articles[2] -> post_title ?></h3>
											<h5 class="east-side"><i class="fas fa-angle-double-right"></i> East Side</h5>
										</div>
										<img src="<?= $articles[2] -> post_cover[0] ?>" alt="">
										<?php endif ?>
									</a>
								</div>
								
							</div>
							
						</div>		
						
					</div>

				</div>
				
			</div>
			
		</div>
		
	</section>

	<section>

		<div class="feed">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-6">

						<div class="main-article">

							<?php if ($articles[3]): ?>							

							<div class="cover">
								<img src="<?= $articles[3] -> post_cover[0] ?>" alt="">
							</div>

							<a href="<?= site_url() ?>/seattle/<?= $articles[3] -> post_name ?>"><h3><?= substr($articles[3] -> post_title, 0, 46) ?></h3></a>

							<h5 class="seattle"><i class="fas fa-map-marker-alt"></i> Seattle</h5>

							<p><?= substr(strip_tags(html_entity_decode($articles[3] -> post_content)), 0, 350) ?>...</p>
								
							<?php endif ?>
							
						</div>
						
					</div>

					<div class="col-xs-12 col-sm-3">

						<?php for($i=4;$i<8;$i++): ?>

						<div class="row">

							<div class="col-xs-12">

								<div class="side-article">

									<h5 class="seattle"><i class="fas fa-map-marker-alt"></i> Seattle</h5>

									<a href="<?= site_url() ?>/seattle/<?= $articles[$i] -> post_name ?>"><h3><?= $articles[$i] -> post_title ?></h3></a>

									<p class="details">Written by <?= get_the_author_meta('nickname', $articles[$i] -> post_author) ?> <?= date('d, M', strtotime($articles[$i] -> post_date)) ?></p>
									
								</div>
								
							</div>
							
						</div>
							
						<?php endfor ?>
						
					</div>

					<div class="col-xs-12 col-sm-3">

						<?php for($i=8;$i<12;$i++): ?>

						<div class="row">

							<div class="col-xs-12">

								<div class="side-article">

									<h5 class="east-side"><i class="fas fa-angle-double-right"></i> East Side</h5>

									<a href="<?= site_url() ?>/seattle/<?= $articles[$i] -> post_name ?>"><h3><?= $articles[$i] -> post_title ?></h3></a>

									<p class="details">Written by <?= get_the_author_meta('nickname', $articles[$i] -> post_author) ?> <?= date('d, M', strtotime($articles[$i] -> post_date)) ?></p>
									
								</div>
								
							</div>
							
						</div>
							
						<?php endfor ?>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</section>

	<section>

		<div class="ad">

			<div class="container">

				<div class="row">

					<div class="col-xs-12">

						<a href="http://seattlepublicrelations.com" target="_blank"><img src="<?= get_template_directory_uri() ?>/img/ad1.gif" alt=""></a>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</section>

	<section>

		<div class="feed">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-3">

						<?php for($i=12;$i<16;$i++): ?>

						<div class="row">

							<div class="col-xs-12">

								<div class="side-article">

									<h5 class="entertainment"><i class="fab fa-youtube"></i> Entertainment</h5>

									<a href="<?= site_url() ?>/seattle/<?= $articles[$i] -> post_name ?>"><h3><?= $articles[$i] -> post_title ?></h3></a>

									<p class="details">Written by <?= get_the_author_meta('nickname', $articles[$i] -> post_author) ?> <?= date('d, M', strtotime($articles[$i] -> post_date)) ?></p>
									
								</div>
								
							</div>
							
						</div>
							
						<?php endfor ?>
						
					</div>

					<div class="col-xs-12 col-sm-6">

						<div class="row">

							<div class="col-xs-12">

								<?php if ($articles[17]): ?>

								<div class="main-article">

									<div class="cover">
										<img src="<?= $articles[17] -> post_cover[0] ?>" alt="">
									</div>

									<a href="<?= site_url() ?>/seattle/<?= $articles[17] -> post_name ?>"><h3><?= $articles[17] -> post_title ?></h3></a>

									<h5 class="entertainment"><i class="fab fa-youtube"></i> Entertainment</h5>
									
								</div>
									
								<?php endif ?>
								
							</div>
							
						</div>
						
					</div>

					<div class="col-xs-12 col-sm-3">

						<?php for($i=18;$i<20;$i++): ?>

						<div class="row">

							<div class="col-xs-12">

								<div class="side-article">

									<div class="cover">
										<img src="<?= $articles[$i] -> post_cover[0] ?>" alt="">
									</div>

									<h5 class="entertainment"><i class="fab fa-youtube"></i> Entertainment</h5>

									<a href="<?= site_url() ?>/seattle/<?= $articles[$i] -> post_name ?>"><h3><?= $articles[$i] -> post_title ?></h3></a>

									<p class="details">Written by <?= get_the_author_meta('nickname', $articles[$i] -> post_author) ?> <?= date('d, M', strtotime($articles[$i] -> post_date)) ?></p>
									
								</div>
								
							</div>
							
						</div>
							
						<?php endfor ?>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</section>

	<script src="<?= get_template_directory_uri() ?>/js/home.js"></script>

<?

get_footer();