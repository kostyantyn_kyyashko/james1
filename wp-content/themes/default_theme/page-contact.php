<?

get_header();

?>

	<section>

		<div class="contact-page-banner">

			
		</div>
		
	</section>

	<section>

		<div class="contact-page">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-6">

						<h2>About Bellevue Chronicle</h2>

						<p>For more than 34 years, Bellevue Chronicle's experienced writers, editors, and designers have captured all sides of our growing city with amazing one of a kind writing, and groundbreaking reporting. Our features, narratives, and investigative news stories tell our two Hundred Thousand monthly online readers just how amazing Bellevue really is.</p>

						<p>We bring a timely, local take to matters of travel, shopping, money, food, fashion, home, and health, through our broadcast partners.</p>

						<p>The Bellevue Chronicle has gone GREEN!</p>

						<p>As of August 12th, 2012, we will no longer sell hard copies of our magazine at retail locations.</p>
						
					</div>

					<div class="col-xs-12 col-sm-6">

						<h1>Contact Us</h1>

						<p>We welcome your feedback to Bellevue Chronicle. We welcome your new stories! If you have a question, comment, suggestion or a NEWS TIP, please email us below:</p>

						<p class="contact-details">Bellevue Chronicle<br>
							7 102nd ave Ne, Bellevue, Wa 98005<br>
							Daniel Brown, Editor in Chief<br>
							<strong>press@bellevuechronicle.com</strong><br>
							425-449-4600<br>
							503-664-0077
						</p>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</section>

	<script src="<?= get_template_directory_uri() ?>/js/home.js"></script>

<?

get_footer();