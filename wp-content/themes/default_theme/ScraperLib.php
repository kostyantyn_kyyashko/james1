<?php

class ScraperLib {

	static public function scrapeURL($url, $params = null, $method='get', $cookie = null, $header = null, $auth = null) {

		switch ($method) {
			case 'get':

				# Initiate CURL

				$ch = curl_init();

				if($params) {

					$params = http_build_query($params);

					curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);

				}else{

					curl_setopt($ch, CURLOPT_URL, $url);

				}

				if($cookie) curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: " . $cookie));

				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");			

				$result = curl_exec($ch);

				return $result;
				
				break;

			case 'post':

				$cookie = '';

				# Initiate CURL

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, $url);

				if($header) curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_POST, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");			

				$result = curl_exec($ch);

				return $result;
				
				break;
		}

	}

}