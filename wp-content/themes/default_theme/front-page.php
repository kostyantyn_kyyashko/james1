<?

$post = get_post('home');

get_header();

?>

	<?

		$featuredArticles = get_posts(array(
			'posts_per_page'	=> -1,
			'post_type'			=> 'post',
			'meta_key'			=> 'featured',
			'meta_value'		=> true
		));

		$articles = new WP_Query('post_type=post&posts_per_page=32');
		$articles = (array) $articles -> posts;

		for ($j=0; $j < count($articles); $j++) { 

			$html = html_entity_decode($articles[$j] -> post_content);

			$doc = new DOMDocument();
			$doc -> loadHTML($html);

			$xpath = new DOMXPath($doc);

			$images = $xpath -> query('//img');			

			$articles[$j] -> post_cover = [];

			foreach ($images as $img) {

				$articles[$j] -> post_cover[] = $img -> getAttribute('src');
				
			}

			unset($images);
			
		}

	?>

	<style>

		.banner {
			background: <?= $themeSettings['ds_banner_background_color'] ?>;
		}

		.banner .side-article h3, .banner .main-article h3 {
			color: <?= $themeSettings['ds_banner_title_font_color'] ?>;
		}

		.banner .side-article h3 {
			background: <?= $themeSettings['ds_banner_background_color'] ?>;
		}
		
	</style>

	<? include('inc/banner_template' . $themeSettings['ds_banner_template'] . '.php'); ?>

	<? include('inc/feed_template' . $themeSettings['ds_feed_template'] . '.php'); ?>

	<!-- <section>

		<div class="ad">

			<div class="container">

				<div class="row">

					<div class="col-xs-12">

						<a href="http://seattlepublicrelations.com" target="_blank"><img src="<?= get_template_directory_uri() ?>/img/ad1.gif" alt=""></a>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</section>

	<section>

		<div class="feed">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-3">

						<?php for($i=12;$i<16;$i++): ?>

						<div class="row">

							<div class="col-xs-12">

								<div class="side-article">

									<h5 class="entertainment"><i class="fab fa-youtube"></i> Entertainment</h5>

									<a href="<?= site_url() ?>/seattle/<?= $articles[$i] -> post_name ?>"><h3><?= $articles[$i] -> post_title ?></h3></a>

									<p class="details">Written by <?= get_the_author_meta('nickname', $articles[$i] -> post_author) ?> <?= date('d, M', strtotime($articles[$i] -> post_date)) ?></p>
									
								</div>
								
							</div>
							
						</div>
							
						<?php endfor ?>
						
					</div>

					<div class="col-xs-12 col-sm-6">

						<div class="row">

							<div class="col-xs-12">

								<?php if ($articles[17]): ?>

								<div class="main-article">

									<div class="cover">
										<img src="<?= $articles[17] -> post_cover[0] ?>" alt="">
									</div>

									<a href="<?= site_url() ?>/seattle/<?= $articles[17] -> post_name ?>"><h3><?= $articles[17] -> post_title ?></h3></a>

									<h5 class="entertainment"><i class="fab fa-youtube"></i> Entertainment</h5>
									
								</div>
									
								<?php endif ?>
								
							</div>
							
						</div>
						
					</div>

					<div class="col-xs-12 col-sm-3">

						<?php for($i=18;$i<20;$i++): ?>

						<div class="row">

							<div class="col-xs-12">

								<div class="side-article">

									<div class="cover">
										<img src="<?= $articles[$i] -> post_cover[0] ?>" alt="">
									</div>

									<h5 class="entertainment"><i class="fab fa-youtube"></i> Entertainment</h5>

									<a href="<?= site_url() ?>/seattle/<?= $articles[$i] -> post_name ?>"><h3><?= $articles[$i] -> post_title ?></h3></a>

									<p class="details">Written by <?= get_the_author_meta('nickname', $articles[$i] -> post_author) ?> <?= date('d, M', strtotime($articles[$i] -> post_date)) ?></p>
									
								</div>
								
							</div>
							
						</div>
							
						<?php endfor ?>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</section> -->

	<script src="<?= get_template_directory_uri() ?>/js/home.js"></script>

<?

get_footer();