<?

$featured = get_field('featured', $post -> ID);

get_header();

$articles = new WP_Query('post_type=post&posts_per_page=6');
$articles = (array) $articles -> posts;

for ($j=0; $j < count($articles); $j++) { 

	$html = html_entity_decode($articles[$j] -> post_content);

	$doc = new DOMDocument();
	$doc -> loadHTML($html);

	$xpath = new DOMXPath($doc);

	$images = $xpath -> query('//img');			

	$articles[$j] -> post_cover = [];

	foreach ($images as $img) {

		$articles[$j] -> post_cover[] = $img -> getAttribute('src');
		
	}

	unset($images);
	
}

?>

	<section>

		<div class="article-page">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-8">

						<div class="row">

							<div class="col-xs-12">

								<h1><?= $post -> post_title ?></h1>
								
							</div>
							
						</div>

						<div class="row">

							<div class="col-xs-12">

								<div class="article-content">

									<?= html_entity_decode($post -> post_content) ?>
									
								</div>
								
							</div>
							
						</div>
						
					</div>

					<div class="col-xs-12 col-sm-4">

						<div class="latest-articles">

							<h3>Trending now</h3>

							<ul>
								<?php foreach ($articles as $article): ?>
								<li>
									<div class="pic">
										<img src="<?= $article -> post_cover[0] ?>" alt="">
									</div>
									<h4><a href="<?= site_url(); ?>/seattle/<?= $article -> post_name ?>"><?= $article -> post_title ?></a></h4>
								</li>								
								<?php endforeach ?>
							</ul>
							
						</div>
						
					</div>
					
				</div>

				<div class="row">

					<div class="col-xs-12">

						<div class="fb-comments" data-href="<?= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" data-numposts="5"></div>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</section>

	<section>
		
	</section>

	<script src="<?= get_template_directory_uri() ?>/js/home.js"></script>

<?

get_footer();