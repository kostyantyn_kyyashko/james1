<section>

	<div class="banner">

		<div class="container<?= ($themeSettings['ds_banner_fluid']) ? '-fluid' : '' ?>">

			<div class="row">

				<div class="col-xs-12 clear-pad-both">

					<div class="row">

						<?php for ($i=0;$i<8;$i++): ?>
						<div class="col-xs-12 col-sm-3 clear-pad-both">

							<div class="side-article">
								<a href="<?= site_url() ?>/eastside/<?= $articles[$i] -> post_name ?>">
									<?php if ($articles[$i]): ?>
									<div class="details">
										<h3><?= $articles[$i] -> post_title ?></h3>
									</div>
									<?php if (!$themeSettings['ds_banner_hide_images']): ?>
									<img src="<?= $articles[$i] -> post_cover[0] ?>" alt="">
									<?php endif ?>
									<?php endif ?>	
								</a>
							</div>
							
						</div>							
						<?php endfor; ?>
						
					</div>	
					
				</div>

			</div>
			
		</div>
		
	</div>
	
</section>