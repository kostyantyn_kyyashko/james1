<header>

	<div class="header">

		<div class="container<?= ($themeSettings['ds_header_fluid']) ? '-fluid' : '' ?>">

			<div class="row">

				<div class="col-xs-12 col-sm-3">

					<a href="<?= home_url(); ?>" class="logo"><? bloginfo('name') ?></a>

					<a href="javascript:void(0)" class="menu-bt"><i class="fa fa-bars"></i></a>
					
				</div>

				<div class="col-xs-12 col-sm-9">

					<nav>
						<ul class="social-nav">
							<li><a href="https://www.facebook.com/BellevueChronicle/"><i class="fab fa-facebook"></i></a></li>								
							<li><a href="mailto:press@bellevuechronicle.com"><i class="fas fa-envelope"></i></a></li>
						</ul>
					</nav>

					<nav>
						<ul class="nav">
							<?php for ($i=0;$i<count($categories);$i++): ?>

							<li>
								<a href="<?= site_url() ?>/<?= $categories[$i] -> slug ?>"><?= $categories[$i] -> name ?></a>
							</li>

							<?php if (($i + 1)<count($categories)): ?>

							<li class="separator"><span></span></li>

							<?php endif ?>

							<?php endfor ?>									
						</ul>
					</nav>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
</header>

