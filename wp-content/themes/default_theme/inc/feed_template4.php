<section>

	<div class="feed f-t4">

		<div class="container<?= ($themeSettings['ds_feed_fluid']) ? '-fluid' : '' ?>">

			<div class="row">

				<div class="col-xs-12 col-sm-8">

					<div class="row">

						<div class="col-xs-12">

							<div class="main-article">

								<?php if ($articles[3]): ?>							

								<div class="cover">
									<img src="<?= $articles[3] -> post_cover[0] ?>" alt="">
								</div>

								<a href="<?= site_url() ?>/seattle/<?= $articles[3] -> post_name ?>"><h3><?= substr($articles[3] -> post_title, 0, 46) ?></h3></a>
									
								<?php endif ?>
								
							</div>
							
						</div>
						
					</div>

					<div class="row">

						<div class="col-xs-12 col-sm-3">

							<div class="main-article main-article-smaller">

								<?php if ($articles[4]): ?>							

								<div class="cover">
									<img src="<?= $articles[4] -> post_cover[0] ?>" alt="">
								</div>

								<a href="<?= site_url() ?>/seattle/<?= $articles[4] -> post_name ?>"><h3><?= substr($articles[4] -> post_title, 0, 46) ?></h3></a>						
									
								<?php endif ?>
								
							</div>
							
						</div>

						<div class="col-xs-12 col-sm-3 clear-pad-both">

							<div class="main-article main-article-smaller">

								<?php if ($articles[5]): ?>							

								<div class="cover">
									<img src="<?= $articles[5] -> post_cover[0] ?>" alt="">
								</div>

								<a href="<?= site_url() ?>/seattle/<?= $articles[5] -> post_name ?>"><h3><?= substr($articles[5] -> post_title, 0, 46) ?></h3></a>						
									
								<?php endif ?>
								
							</div>
							
						</div>

						<div class="col-xs-12 col-sm-3 clear-pad-right">

							<div class="main-article main-article-smaller">

								<?php if ($articles[6]): ?>							

								<div class="cover">
									<img src="<?= $articles[6] -> post_cover[0] ?>" alt="">
								</div>

								<a href="<?= site_url() ?>/seattle/<?= $articles[6] -> post_name ?>"><h3><?= substr($articles[6] -> post_title, 0, 46) ?></h3></a>						
									
								<?php endif ?>
								
							</div>
							
						</div>

						<div class="col-xs-12 col-sm-3">

							<div class="main-article main-article-smaller">

								<?php if ($articles[7]): ?>							

								<div class="cover">
									<img src="<?= $articles[7] -> post_cover[0] ?>" alt="">
								</div>

								<a href="<?= site_url() ?>/seattle/<?= $articles[7] -> post_name ?>"><h3><?= substr($articles[7] -> post_title, 0, 46) ?></h3></a>						
									
								<?php endif ?>
								
							</div>
							
						</div>
						
					</div>
					
				</div>

				<div class="col-xs-12 col-sm-4">

					<?php for($i=8;$i<15;$i++): ?>

					<div class="row">

						<div class="col-xs-12">

							<div class="side-article">

								<a href="<?= site_url() ?>/seattle/<?= $articles[$i] -> post_name ?>"><h3><?= $articles[$i] -> post_title ?></h3></a>

								<p class="details">By <strong><?= get_the_author_meta('nickname', $articles[$i] -> post_author) ?></strong></p>
								
							</div>
							
						</div>
						
					</div>
						
					<?php endfor ?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
</section>