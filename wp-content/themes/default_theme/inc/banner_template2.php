<section>

	<div class="banner b-t2">

		<div class="container<?= ($themeSettings['ds_banner_fluid']) ? '-fluid' : '' ?>">

			<div class="row">

				<div class="col-xs-12 clear-pad-both">

					<div class="row half-pad">

						<div class="col-xs-12 col-sm-6 clear-pad-both">

							<div class="main-article">
								<a href="<?= site_url() ?>/eastside/<?= $articles[$i] -> post_name ?>">
									<?php if ($articles[0]): ?>
									<div class="details">
										<h3><?= $articles[0] -> post_title ?></h3>
									</div>
									<?php if (!$themeSettings['ds_banner_hide_images']): ?>
									<img src="<?= $articles[0] -> post_cover[0] ?>" alt="">
									<?php endif ?>
									<?php endif ?>	
								</a>
							</div>							
							
						</div>

						<?php for ($i=1;$i<5;$i++): ?>
						<div class="col-xs-12 col-sm-3 clear-pad-both">

							<div class="side-article">
								<a href="<?= site_url() ?>/eastside/<?= $articles[$i] -> post_name ?>" title="<?= $articles[$i] -> post_title ?>">
									<?php if ($articles[$i]): ?>
									<div class="details">
										<h3><?= substr($articles[$i] -> post_title, 0, 40) ?>...</h3>
									</div>
									<?php if (!$themeSettings['ds_banner_hide_images']): ?>
									<img src="<?= $articles[$i] -> post_cover[0] ?>" alt="">
									<?php endif ?>
									<?php endif ?>	
								</a>
							</div>
							
						</div>							
						<?php endfor; ?>
						
					</div>	
					
				</div>

			</div>
			
		</div>
		
	</div>
	
</section>