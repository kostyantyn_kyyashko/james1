<section>

	<div class="banner b-t3">

		<div class="container<?= ($themeSettings['ds_banner_fluid']) ? '-fluid' : '' ?>">

			<div class="row">

				<div class="col-xs-12">

					<div class="row">

						<div class="col-xs-12 col-sm-6 clear-pad-right">

							<div class="main-article">
								<a href="<?= site_url() ?>/eastside/<?= $articles[$i] -> post_name ?>">
									<?php if ($articles[0]): ?>
									<div class="details">
										<h3><?= $articles[0] -> post_title ?></h3>
									</div>
									<?php if (!$themeSettings['ds_banner_hide_images']): ?>
									<img src="<?= $articles[0] -> post_cover[0] ?>" alt="">
									<?php endif ?>
									<?php endif ?>	
								</a>
							</div>							
							
						</div>

						<div class="col-xs-12 col-sm-3 clear-pad-right">

							<div class="side-article">
								<a href="<?= site_url() ?>/eastside/<?= $articles[1] -> post_name ?>">
									<?php if ($articles[1]): ?>
									<div class="details">
										<h3><?= $articles[1] -> post_title ?></h3>
									</div>
									<?php if (!$themeSettings['ds_banner_hide_images']): ?>
									<img src="<?= $articles[1] -> post_cover[0] ?>" alt="">
									<?php endif ?>
									<?php endif ?>	
								</a>
							</div>
							
						</div>

						<div class="col-xs-12 col-sm-3">

							<div class="side-article">
								<a href="<?= site_url() ?>/eastside/<?= $articles[2] -> post_name ?>">
									<?php if ($articles[2]): ?>
									<div class="details">
										<h3><?= $articles[2] -> post_title ?></h3>
									</div>
									<?php if (!$themeSettings['ds_banner_hide_images']): ?>
									<img src="<?= $articles[2] -> post_cover[0] ?>" alt="">
									<?php endif ?>
									<?php endif ?>	
								</a>
							</div>
							
						</div>

						<div class="col-xs-12 col-sm-3 clear-pad-right">

							<div class="side-article">
								<a href="<?= site_url() ?>/eastside/<?= $articles[3] -> post_name ?>">
									<?php if ($articles[3]): ?>
									<div class="details">
										<h3><?= $articles[3] -> post_title ?></h3>
									</div>
									<?php if (!$themeSettings['ds_banner_hide_images']): ?>
									<img src="<?= $articles[3] -> post_cover[0] ?>" alt="">
									<?php endif ?>
									<?php endif ?>	
								</a>
							</div>
							
						</div>

						<div class="col-xs-12 col-sm-3">

							<div class="side-article">
								<a href="<?= site_url() ?>/eastside/<?= $articles[4] -> post_name ?>">
									<?php if ($articles[4]): ?>
									<div class="details">
										<h3><?= $articles[4] -> post_title ?></h3>
									</div>
									<?php if (!$themeSettings['ds_banner_hide_images']): ?>
									<img src="<?= $articles[4] -> post_cover[0] ?>" alt="">
									<?php endif ?>
									<?php endif ?>	
								</a>
							</div>
							
						</div>
						
					</div>	
					
				</div>

			</div>
			
		</div>
		
	</div>
	
</section>