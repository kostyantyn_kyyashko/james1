var App = Class.extend({

	sk: null,

	init: function() {

		var self = this;

		self.bindAnimations();

		self.bindActions();

	},

	bindAnimations: function() {

		var self = this;

		$('.menu-bt').on('click', function() {

			$('.nav').toggle();

		})
		
	},

	bindActions: function() {

		var self = this;

	}

})

window.onload = function() {

	var app = new App();

}