var App = Class.extend({

	sk: null,

	init: function() {

		var self = this;

		self.bindAnimations();

		self.bindActions();

	},

	bindAnimations: function() {

		var self = this;

		var sr = ScrollReveal({ duration: 3000, scale: 1 });

		sr.reveal('.reveal');

		sr.reveal('.reveal2', { delay: 500 });
		sr.reveal('.reveal4', { delay: 1000 });
		sr.reveal('.reveal5', { delay: 1500 });
		sr.reveal('.reveal6', { delay: 2000 });

		if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
		    skrollr.init({
		        forceHeight: false
		    });
		}

		$('.open-menu-bt').on('click', function() {

			$('.side-menu, .overlay').fadeIn();

		})

		$('.overlay').on('click', function() {

			$('.side-menu, .overlay').fadeOut();

		})

		$('.team-members .team-member').hover(function() {

			$(this).find('img').stop().fadeOut(500);

		}, function() {

			$(this).find('img').stop().fadeIn(1000);

		})

		$('.nav li').hover(function() {

			$(this).find('ul').stop().fadeIn();

		}, function() {

			$(this).find('ul').stop().fadeOut();

		})
		
	},

	bindActions: function() {

		var self = this;

	}

})

window.onload = function() {

	var app = new App();

}