<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
$data = xhprof_disable();
function save_html($data)
{
    arsort($data);
    $html = <<<html
<html>
<body>
<table style="border: 1px solid grey;">
<thead>
<tr><th>Function</th><th>Time</th></tr>
</thead>
<tbody>
html;

    if(is_array($data)) {
        $out = [];
        foreach ($data as $main => $sub_array) {
            if (strstr($main, 'main()==>')) {
                $function_name = str_replace('main()==>', mt_rand(1000, 9999) . '__' , $main);
            }
            else {
                $function_name = mt_rand(1000, 9999) . '__' . $main;
            }
            $time = intval($sub_array['wt']);
            $out[$function_name] = $time;
            }
        arsort($out);
        foreach ($out as $function_name => $time) {
            $row = "<tr><td>{$function_name}</td><td>{$time}</td></tr>";
            $html .= $row;
        }
    }
    $html .= "</table></tbody></body></html>";
    file_put_contents('/var/www/wp3.nottes.net/xd.html', $html);
}
save_html($data);